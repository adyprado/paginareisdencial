<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Creacion de Estados';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
           
                <?php
                $form = ActiveForm::begin();
                ?>
                
                <?php echo $form->field($model, 'estados')->textInput()->hint('Indique el Nombre del Estado')->label('estados'); ?>

                <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary', 'name' => 'guardar_ciudad']) ?>
                </div>
                
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>