<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Asignacion de Ciudades';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
           
                <?php
                $form = ActiveForm::begin();
                ?>
                
                <?php 
                    $listData=ArrayHelper::map($estados,'id','estados');
                    echo $form->field($model, 'Estados')->dropDownList($listData,['prompt'=>'Select...']);
                    echo $form->field($model, 'ciudades')->textInput()->hint('Indique el Nombre de la ciudad')->label('Ciudad');
                ?>



                

                <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary', 'name' => 'guardar_ciudad']) ?>
                </div>
                
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>