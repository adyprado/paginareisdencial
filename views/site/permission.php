<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Usted <strong>NO</strong> se encuentra autorizado para la siguiente accion.
    </p>
    <p>
        Porfavor contactenos si considera que esto es un error. Gracias.
    </p>

</div>
