<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'La Empresa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p> Nuestra principal actividad es la ADMINISTRACIÓN DE CONDOMINIOS, 
                    y  de una manera general exponemos los servicios que ofrecemos, 
                    de acuerdo con las necesidades de cada  Edificio.</p>
                <p>Además de la amplia trayectoria y experiencia de quince (15) años de su principal
                    Directivos contamos y ponemos a su disposición nuestras oficinas,
                    igualmente nos complace informarles que <strong>INMOBILIARIA PRADO 2010, C. A.</strong>,
                    cuenta con equipos propios para el procesamiento de datos 
                    y brindarle a nuestros clientes la mayor atención y satisfacción posible.</p>
                <p>
                    Sabemos que si trabajamos en conjunto,
                    Junta de Condominio, Comunidad e  <strong>INMOBILIARIA PRADO 2010, C. A.</strong>,
                    podremos garantizar que su Edificio será el mejor cuidado,
                    mantenido y revalorizado, por lo tanto sus apartamentos,
                    locales y oficinas,
                    cada día tendrán un mayor valor en el Mercado Inmobiliario. 
                </p>
            </div>
            <div class="col-md-6">
                <ul class="list-group">
                    <strong>NOS ENCARGAMOS DE LOS PAGOS DE: </strong>
                    <p>
                    <li class="list-group-item">Empleados del Edificio.</li>
                    <li class="list-group-item">C. A. Electricidad de Caracas.</li>
                    <li class="list-group-item">Hidrocapital.</li>
                    <li class="list-group-item">Mantenimiento Ascensores.</li>
                    <li class="list-group-item">Mantenimiento Sistema Hidroneumático.</li>
                    <li class="list-group-item">Mantenimiento Puertas Automáticas, Mantenimientos en General.</li>
                    <li class="list-group-item">Seguro Social Obligatorio.</li>
                    <li class="list-group-item">Ley Política Habitacional.</li>
                    <li class="list-group-item">Facturas Proveedores.</li>
                    <li class="list-group-item">Facturas Contratistas.</li>
                    <li class="list-group-item">Servicio de Elaboracion de recibos de Condominio y Contabilidad, ejerciendo control de cobranzas de la Junta de Condominio.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
