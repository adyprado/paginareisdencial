<?php
//use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\growl\Growl;

$this->title = 'Modulo de Publicacion de Inmuebles';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
            <?php
            echo Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? Html::encode($message['message']) : '',
                'showSeparator' => true,
                'delay' => 3, //This delay is how long before the message shows
                'pluginOptions' => [
                    'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            ?>
<?php endforeach; ?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <strong>Atencion!</strong> Todos los campos son requeridos.
            </div>
        </div>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <div class="row">
            <h3>Informacion Inmueble</h3>
            <div class="col-md-6">
                <?php 
                    $listDataEstados=ArrayHelper::map($modelEstado,'id','estados');
                    echo $form->field($estado, 'Estados')->dropDownList($listDataEstados,['prompt'=>'Select...', 'id'=>'estado_select_publicacion']);
                ?>  
            </div>
            <div class="col-md-6">
                <?php
                    echo $form->field($ciudades, 'Ciudades')->dropDownList($ciudades,['prompt'=>'Select...', 'id'=>'ciudad_select_publicacion']);
                ?>
            </div>
        </div>
        <div id="inputs_publicacion" style="display: none;">
            <div class="row">
                <div class="col-md-2">
                    <?php echo $form->field($publicacion, 'nombreEdificio')->textInput()->hint('Indique el Nombre del Edificio')->label('Edificio'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->field($publicacion, 'piso')->textInput()->hint('Piso Apt')->label('Piso:'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->field($publicacion, 'costo')->textInput()->hint('En Bolivares')->label('Precio:'); ?>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->field($publicacion, 'metros')->textInput()->hint('Metros Cuadrados')->label('Dimension del apt:'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($publicacion, 'cuartos')->textInput()->hint('')->label('Numero de Cuartos'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($publicacion, 'banos')->textInput()->hint('')->label('Numero de Banos:'); ?>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->field($publicacion, 'estacionamiento')->textInput()->hint('')->label('# Estacionamiento:'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($publicacion, 'antiguedad')->textInput()->hint('')->label('Antiguedad:'); ?>
                        </div>
                        <div class="col-md-4">
                            <?php
                                $tipoPublicacionArray=ArrayHelper::map($tipoPublicacion,'id','tipo_anuncio');
                                echo $form->field($tipo, 'tipo_anuncio')->dropDownList($tipoPublicacionArray,['prompt'=>'Select...', 'id'=>'tipo_publicacion']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
                </div>
            </div>
            
            <div class="row">
                <h3>Informacion Venedor</h3>    
                <div class="col-md-2">
                    <?php echo $form->field($publicacion, 'nombre')->textInput()->hint('')->label('Nombre:'); ?>
                </div>
                <div class="col-md-2">
                    <?php echo $form->field($publicacion, 'cedula')->textInput()->hint('')->label('CI o RIF:'); ?>
                </div>
            </div>
            <div class="row">
                <h5>Telefonos de Contactos:</h5>
                <div class="col-md-3">
                    <?php echo $form->field($publicacion, 'telefonoLocal')->textInput()->hint('')->label('Local:'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->field($publicacion, 'telefonoCelular')->textInput()->hint('')->label('Celular:'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                      <?php echo $form->field($publicacion, 'correo')->textInput()->hint('')->label('Correo:'); ?>
                </div>
            </div>   
        </div>
        <div class="modal fade" id="modal_publicacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Guardar Publicacion</h4>
                    </div>
                    <div class="modal-body">
                        ¿Esta seguro desea guardar la siguiente publicacion?...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>  
        </div>
        
    </div>
<button id="modal_show_publicacion" type="submit" class="btn btn-default btn-lg" aria-label="Left Align">
            Guardar <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
</button>

