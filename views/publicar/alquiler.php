<?php
//use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\growl\Growl;

$this->title = 'Alquiler';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if(!$alquiler): ?>
  <div class="jumbotron">
  <h2>Lo sentimos, no hay alquileres publicados por el momento....</h2>
  <p><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></p>
  </div>
<?php else:?>
    <div class="dynatable-ventas ">
      <ul id="tabla_publicacion_venta" class="row-fluid">
        <?php foreach ($alquiler as $key => $value){ ?>
          <li class="span4" data-color="gray">
          <div class="thumbnail">
            <div class="thumbnail-image">
                <?php 
                    $image = json_decode($value->foto,true)[1];
                    echo Html::img('@web/'.$image,["style"=>'width: 253px; height: 158px']); 
                ?>
            </div>
            <div class="caption">
              <h3> <?php echo "Estado:".$value->estado ?></h3>
              <p><?php echo "Ciudad:".$value->ciudad ?></p>
              <p><?php echo "Valor:".$value->monto ?></p>
              <p><a id="revizar_venta" class="btn btn-primary modal_show_sale" value="<?php echo $value->post ?>">View</a>
            </div>
          </div>
          </li>
        <?php } ?>
      </ul>
    </div>       
<?php endif; ?>

<div class="modal fade bs-example-modal-lg" id="modal_publicacion_sale_show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_titulo_publicacion"> </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-6" id="foto_grid"> 
                         
                     </div>
                     <div class="col-md-6" id="information">
                         
                     </div>  
                </div>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

	


