<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
 
    NavBar::begin([
        'brandLabel' => '<div style="margin-top: -10px;"><img width="45" class="pull-left"  src="/images/logo___copia_1.png"/></div>'.'<div style="margin-top: 10px; margin-left: 55px;"> INMOBILIARIA PRADO 2010, C. A.</div>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    
    if (Yii::$app->user->isGuest) {
        $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                ['label' => 'Publicaciones', 'items' => [
                                ['label' => 'Ventas', 'url' => ['/publicar/ventas']],
                                ['label' => 'Alquiler', 'url' => ['/publicar/alquiler']],
                                ],],
            ];
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        if (Yii::$app->user->identity->type_user_id == 1)
        {
            $menuItems[] = ['label'=> 'Residencias','url' => ['/residencias']];
             $menuItems[] = 
                    ['label' => 'Publicar', 'url' => ['/publicar/upload']
                 ];
        }
        $menuItems[] = [
            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']];
        
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <div class="container">
        <p class="pull-left">&copy;INMOBILIARIA PRADO 2010, C. A. <?= date('Y') ?></p>
        <p class="pull-right"><?= "Hecho por Ady A. Prado S."; ?></p>
    </div>
</footer>
   
<?php $this->endBody() ?>

     <script>
        jQuery(document).ready(function () {
            $CONDOMINIO.UI.init();
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>
