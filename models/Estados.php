<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estados".
 *
 * @property integer $id
 * @property string $estados
 *
 * @property Ciudades[] $ciudades
 */
class Estados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public $Estados;
    
    public static function tableName()
    {
        return 'estados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estados'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estados' => 'Estados',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudades()
    {
        return $this->hasMany(Ciudades::className(), ['id_estados' => 'id']);
    }
}
