<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "owner_information".
 *
 * @property integer $id
 * @property string $cedula
 * @property string $correo
 * @property string $nombre
 * @property string $telefonoLocal
 * @property string $telefonoCelular
 *
 * @property Publicacion[] $publicacions
 */
class OwnerInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'owner_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cedula', 'correo', 'nombre', 'telefonoLocal', 'telefonoCelular'], 'required'],
            [['cedula', 'correo', 'telefonoLocal', 'telefonoCelular'], 'string', 'max' => 45],
            [['nombre'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cedula' => 'Cedula',
            'correo' => 'Correo',
            'nombre' => 'Nombre',
            'telefonoLocal' => 'Telefono Local',
            'telefonoCelular' => 'Telefono Celular',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacions()
    {
        return $this->hasMany(Publicacion::className(), ['owner_information' => 'id']);
    }
}
