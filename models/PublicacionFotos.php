<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicacion_fotos".
 *
 * @property integer $id
 * @property integer $id_publicacion
 * @property string $fotos
 */
class PublicacionFotos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacion_fotos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_publicacion'], 'integer'],
            [['fotos'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_publicacion' => 'Id Publicacion',
            'fotos' => 'Fotos',
        ];
    }
}
