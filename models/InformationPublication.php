<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "information_publication".
 *
 * @property integer $id
 * @property string $nombreEdificio
 * @property string $piso
 * @property string $banos
 * @property string $cuartos
 * @property string $metros
 * @property string $antiguedad
 * @property string $estacionamiento
 * @property string $costo
 *
 * @property Publicacion[] $publicacions
 */
class InformationPublication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'information_publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreEdificio', 'piso', 'banos', 'cuartos', 'metros', 'costo'], 'required'],
            [['id'], 'integer'],
            [['nombreEdificio', 'piso', 'banos', 'cuartos', 'metros', 'antiguedad', 'estacionamiento'], 'string', 'max' => 45],
            [['costo'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreEdificio' => 'Nombre Edificio',
            'piso' => 'Piso',
            'banos' => 'Banos',
            'cuartos' => 'Cuartos',
            'metros' => 'Metros',
            'antiguedad' => 'Antiguedad',
            'estacionamiento' => 'Estacionamiento',
            'costo' => 'Costo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacions()
    {
        return $this->hasMany(Publicacion::className(), ['information' => 'id']);
    }
}
