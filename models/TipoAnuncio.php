<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "tipo_anuncio".
 *
 * @property integer $id
 * @property string $tipo_anunciocol
 */
class TipoAnuncio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_anuncio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_anuncio'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_anuncio' => 'Tipo Anuncio',
        ];
    }
    
    public static function getTipoPublicacion()
    {
        
        $query = new Query();
        $query->select('id,tipo_anuncio')->from('tipo_anuncio');
        
        return $query->all();
    }
}
