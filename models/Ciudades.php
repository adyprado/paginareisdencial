<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "ciudades".
 *
 * @property integer $id
 * @property string $ciudades
 * @property integer $id_estados
 *
 * @property Estados $idEstados
 */
class Ciudades extends \yii\db\ActiveRecord
{
    public $Estados;
    public $Ciudades;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estados'], 'integer'],
            [['ciudades'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ciudades' => 'Ciudades',
            'id_estados' => 'Id Estados',
            'estados' => 'Estados',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstados()
    {
        return $this->hasOne(Estados::className(), ['id' => 'id_estados']);
    }
    
    public static function getCiudadesByID($id)
    {
        
        $query = new Query();
        $query->select('id,ciudades')->from('ciudades')->where("id_estados=".$id);
        
        return $query->all();
    }
}
