<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\PublicacionFotos;

class UploadImages extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg ,jpeg', 'maxFiles' => 6],
        ];
    }
    
    public function upload($id,$id_city)
    {
        if ($this->validate()) 
        { 
            $directorio = 'uploads/'.$id_city.'_'.$id;
            
            if (!is_dir($directorio))
            {
                    $i = 1;
                    $array = [];
                    foreach ($this->imageFiles as $file)
                    {
                        if(!$file->saveAs($directorio."_".$i . '.' . $file->extension))
                        {
                            var_dump("aca falla");
                            exit();
                            break;
                        }
                        else
                        {
                            $array[$i]= $directorio."_".$i . '.' . $file->extension;
                        }
                        $i++;
                    }
                    $publicacionFoto = new PublicacionFotos;
                    $publicacionFoto->id_publicacion = $id;
                    $publicacionFoto->fotos = json_encode($array);
//                    var_dump($publicacionFoto);
//                    exit();
                    if ($publicacionFoto->save())
                        return true;    
                //}
            }

           
            
        } 
        else 
        {
            return false;
        }
    }
}