<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicacion".
 *
 * @property string $id
 * @property string $owner_information
 * @property integer $id_tipo_anuncio
 * @property integer $id_ciudad
 * @property string $information
 * @property string $create_date
 * @property string $end_date
 * @property integer $id_status_publicacion
 *
 * @property Ciudades $idCiudad
 * @property StatusPublicacion $idStatusPublicacion
 * @property TipoAnuncio $idTipoAnuncio
 */
class Publicacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $nombreEdificio;
    public $piso;
    public $metros;
    public $banos;
    public $cuartos;
    public $descripcion;
    public $estacionamiento;
    public $antiguedad;
    public $costo;
    public $nombre;
    public $cedula;
    public $telefonoLocal;
    public $telefonoCelular;
    public $correo;
    public $ciudad;
    public $estado;
    public $monto;
    public $post;
    public $citidi;
    public $foto;
    public $fotos;
    
    
    public static function tableName()
    {
        return 'publicacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_information', 'id_tipo_anuncio', 'id_ciudad', 'information', 'create_date'], 'required'],
            [['owner_information', 'information'], 'integer'],
            [['id_tipo_anuncio', 'id_ciudad', 'id_status_publicacion'], 'integer'],
            [['create_date', 'end_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_information' => 'Owner Information',
            'id_tipo_anuncio' => 'Id Tipo Anuncio',
            'id_ciudad' => 'Id Ciudad',
            'information' => 'Information',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'id_status_publicacion' => 'Id Status Publicacion',
            'nombreEdificio' => 'Nombre Del Edificio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCiudad()
    {
        return $this->hasOne(Ciudades::className(), ['id' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatusPublicacion()
    {
        return $this->hasOne(StatusPublicacion::className(), ['id' => 'id_status_publicacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoAnuncio()
    {
        return $this->hasOne(TipoAnuncio::className(), ['id' => 'id_tipo_anuncio']);
    }
    
    public static function getJSONowner($array)
    {
        $json = [];
        $json["owner_information"]["nombre"]=$array["nombre"];
        $json["owner_information"]["cedula"]=$array["cedula"];
        $json["owner_information"]["telefonoLocal"]=$array["telefonoLocal"];
        $json["owner_information"]["telefonoCelular"]=$array["telefonoCelular"];
        $json["owner_information"]["correo"]=$array["correo"];
        
        return json_encode($json);
    }
    
    public static function getJSONinformation($array)
    {
        $json = [];
        $json["owner_information"]["nombreEdificio"]=$array["nombreEdificio"];
        $json["owner_information"]["piso"]=$array["piso"];
        $json["owner_information"]["costo"]=$array["costo"];
        $json["owner_information"]["metros"]=$array["metros"];
        $json["owner_information"]["cuartos"]=$array["cuartos"];
        $json["owner_information"]["banos"]=$array["banos"];
        $json["owner_information"]["estacionamiento"]=$array["estacionamiento"];
        $json["owner_information"]["antiguedad"]=$array["antiguedad"];
        
        return json_encode($json);
    }
    
    public static function getAllSales()
    {
        
        $sql ="SELECT p.id post,c.id citidi,i.costo monto,c.ciudades ciudad,e.estados estado,f.fotos as foto from information_publication as i
                inner join publicacion as p
                on p.information = i.id
                inner join ciudades as c 
                on p.id_ciudad = c.id
                inner join estados as e
                on c.id_estados = e.id
                inner join publicacion_fotos as f
                on f.id_publicacion = p.id
                where p.id_tipo_anuncio =2;" ;
        $sales = self::findBySql($sql)->all();
        return $sales;
    }
    
    public static function getAllRents()
    {
        
        $sql ="SELECT p.id post,c.id citidi,i.costo monto,c.ciudades ciudad,e.estados estado,f.fotos as foto from information_publication as i
                inner join publicacion as p
                on p.information = i.id
                inner join ciudades as c 
                on p.id_ciudad = c.id
                inner join estados as e
                on c.id_estados = e.id
                inner join publicacion_fotos as f
                on f.id_publicacion = p.id
                where p.id_tipo_anuncio =1;" ;
        $rent = self::findBySql($sql)->all();
        return $rent;   
    }
    
    public static function getInformationForModal($id)
    {
        $sql ="select inf.nombreEdificio, inf.piso, inf.banos, inf.cuartos, inf.metros, inf.antiguedad,
                inf.estacionamiento, inf.costo, pfot.fotos , oinf.telefonoCelular, oinf.telefonoLocal              
                from publicacion as p
                inner join information_publication as inf
                on p.information = inf.id
                inner join owner_information as oinf
                on p.owner_information = oinf.id
                inner join publicacion_fotos as pfot
                on pfot.id_publicacion = p.id
                where p.id = {$id};";
        $sales = self::findBySql($sql)->one();
        return $sales;   
    }
        
   
}
