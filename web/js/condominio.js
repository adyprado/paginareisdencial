/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var $CONDOMINIO={};



$CONDOMINIO.UI = (function () {

    function init()
    {
        _create_publication();
        _publication_tables();
    }
    
    function _create_publication()
    {
        $("#estado_select_publicacion").on('change', function(){
            var estado_selected = $("#estado_select_publicacion option:selected").val();
            console.log("El seleccionado es");
            console.log(estado_selected);
            var idDivision = $.ajax({
                            type: "GET",
                            url: "/ciudades/bringstates?id="+estado_selected,
                            async: false,
                            data: estado_selected,
                            success: function(data)
                            {}
                        }).responseText;
            console.log(idDivision);
            var jsonCiudades = JSON.parse(idDivision);
            $('#ciudad_select_publicacion').find('option').remove().end();
            var option = document.createElement("option");
            option.text = "Seleccione Ciudad";
            option.value = null;
            var select = document.getElementById("ciudad_select_publicacion");
            select.appendChild(option);
            console.log(jsonCiudades);
            jQuery.each(jsonCiudades, function(i, val) {
                var option = document.createElement("option");
                option.text = val.ciudades;
                option.value = val.id;
                var select = document.getElementById("ciudad_select_publicacion");
                select.appendChild(option);
            });
       });     
        
        $("#ciudad_select_publicacion").on('change', function(){
            console.log("EPA");
            $('#inputs_publicacion').show();
        }); 
        $("#modal_show_publicacion").on('click', function(){
            console.log("Hola");
            $('#modal_publicacion').modal('show');
        });
  
        
        
    }

    function _publication_tables()
    {
        $('#tabla_publicacion_venta').dynatable({
            table: {
                bodyRowSelector: 'li'
            },
            dataset: {
                perPageDefault: 1,
                perPageOptions: [1,3,6]
            },
            writers: {
                _rowWriter: ulWriter
            },
            readers: {
                _rowReader: ulReader
            },
            params: {
                records: 'inmuebles'
            }
        });
        
        $("a.modal_show_sale").on('click', function(){
            var id = $(this).attr('value');
            $("#carrusel").remove();
            $(".panel").remove();
            $(".espacio").remove();
            console.log(id);
             var infoSales = $.ajax({
                            type: "GET",
                            url: "/publicar/getinfo?id="+id,
                            async: false,
                            data: null,
                            success: function(data)
                            {}
                        }).responseText;
            console.log(infoSales);
            var carousel = '<div id="carrusel" class="carousel slide" data-ride="carousel">';
            var objCarousel = JSON.parse(infoSales);
            console.log(objCarousel);
            console.log('(((())))');
            var fotos= objCarousel.information.fotos;
            var information = objCarousel.information;
            carousel += '<ol class="carousel-indicators">';
            var i=0;
            jQuery.each(fotos,function(){
                if(i==0)
                {
                    carousel += '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
                    i++;
                }
                else
                {
                    carousel += '<li data-target="#carousel-example-generic" data-slide-to="'+i+'" class="active"></li>';    
                }
                
            });
            i=0;
            carousel += '</ol>';
            carousel += '<div class="carousel-inner" role="listbox">';
            console.log(fotos);
            jQuery.each(fotos,function(){
                if(i==0)
                {
                    carousel += '<div class="item active">';
                    carousel += '<img src="/'+fotos[i+1]+'" alt="...">';
                    carousel += '</div>';
                    i++;
                }
                else
                {
                    carousel += '<div class="item">';
                    carousel += '<img src="/'+fotos[i+1]+'" alt="...">';
                    carousel += '</div>';
                    i++;   
                } 
            });
            carousel += '</div>';
            carousel += '<a id="izquierda_carrusel" class="left carousel-control" href="#carrusel" role="button" data-slide="prev">';
            carousel += '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>';
            carousel += '<span class="sr-only">Previous</span>';
            carousel += '</a>';
            carousel += '<a class="right carousel-control" href="#carrusel" role="button" data-slide="next">';
            carousel += '<span id="derecha_carrusel" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>';
            carousel += '<span class="sr-only">Next</span>';
            carousel += '</a>';
            carousel += '</div>';
            carousel += '<br class="espacio">';
            carousel += '<div class="panel panel-default">';
            carousel += '<div class="panel-heading">';
            carousel += '<h3 class="panel-title">Antiguedad Edificacion (Años)</h3>';
            carousel += '</div>';
            carousel += '<div class="panel-body">';
            carousel += information.antiguedad;
            carousel += '</div>';
            carousel += '</div>';
            carousel += '<div class="panel panel-default">';
            carousel += '<div class="panel-heading">';
            carousel += '<h3 class="panel-title">Puesto de Estacionamientos</h3>';
            carousel += '</div>';
            carousel += '<div class="panel-body">';
            carousel += information.estacionamiento;
            carousel += '</div>';
            carousel += '</div>';
            carousel += '<div class="panel panel-default">';
            carousel += '<div class="panel-heading">';
            carousel += '<h3 class="panel-title">Contacto</h3>';
            carousel += '</div>';
            carousel += '<div class="panel-body">';
            carousel += '<strong> Local</strong>:'+information.telefonoLocal+' <strong> Celular</strong>:'+information.telefonoCelular;
            carousel += '</div>';
            carousel += '</div>';
            $("#foto_grid").append(carousel);
            var restInformation ='';
            restInformation += '<div class="panel panel-default">';
            restInformation += '<div class="panel-heading">';
            restInformation += '<h3 class="panel-title">Nombre Edificio</h3>';
            restInformation += '</div>';
            restInformation += '<div class="panel-body">';
            restInformation += information.nombreEdificio;
            restInformation += '</div>';
            restInformation += '</div>';
            restInformation += '<div class="panel panel-default">';
            restInformation += '<div class="panel-heading">';
            restInformation += '<h3 class="panel-title">Piso</h3>';
            restInformation += '</div>';
            restInformation += '<div class="panel-body">';
            restInformation += information.piso;
            restInformation += '</div>';
            restInformation += '</div>';
            restInformation += '<div class="panel panel-default">';
            restInformation += '<div class="panel-heading">';
            restInformation += '<h3 class="panel-title">Metros Cuadrados</h3>';
            restInformation += '</div>';
            restInformation += '<div class="panel-body">';
            restInformation += information.metros;
            restInformation += '</div>';
            restInformation += '</div>';
            restInformation += '<div class="panel panel-default">';
            restInformation += '<div class="panel-heading">';
            restInformation += '<h3 class="panel-title">Cuartos / Baños</h3>';
            restInformation += '</div>';
            restInformation += '<div class="panel-body">';
            restInformation += information.cuartos + ' / ' + information.banos;
            restInformation += '</div>';
            restInformation += '</div>';
            $("#information").append(restInformation);
            $("#carrusel").carousel({
                interval: 2500    
            });
            
            $('#modal_publicacion_sale_show').modal('show');
            
        });
    }
    
    
    function ulWriter(rowIndex, record, columns, cellWriter) 
    {
        var cssClass = "span4", li;
        if (rowIndex % 3 === 0) { cssClass += ' first'; }
        li = '<li class="' + cssClass + '"><div class="thumbnail"><div class="thumbnail-image">' + record.thumbnail + '</div><div class="caption">' + record.caption + '</div></div></li>';
        return li;
    }

    function ulReader(index, li, record) 
    {
        var $li = $(li),
        $caption = $li.find('.caption');
        record.thumbnail = $li.find('.thumbnail-image').html();
        record.caption = $caption.html();
        record.label = $caption.find('h3').text();
        record.description = $caption.find('p').text();
        record.color = $li.data('color');
    }

    return {
            init: init,
        };
    })();

