<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadImages extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }
    
    public function upload($id,$id_city)
    {
        if ($this->validate()) { 
            $directorio = "uploads/".$id_city."/".$id."/";
//            var_dump($directorio);
//            exit();
            mkdir($directorio, 0777, true);
            $i = 1;
            foreach ($this->imageFiles as $file) {
                
                $file->saveAs($directorio. $i . '.' . $file->extension);
                $i++;
            }
            return true;
        } else {
            return false;
        }
    }
}