-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: condominio
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudades` varchar(255) NOT NULL,
  `id_estados` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_estados` (`id_estados`),
  CONSTRAINT `id_estados` FOREIGN KEY (`id_estados`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES (1,'Caracas - Baruta(central)',11),(2,'Caracas - Baruta(sur)',11);
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estados` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (1,'Amazonas'),(2,'Anzoategui'),(3,'Apure'),(4,'Aragua'),(5,'Barinas'),(6,'Bolivar'),(7,'Carabobo'),(8,'Cojedes'),(9,'Delta Amacuro'),(10,'Depend. Federales'),(11,'Distrito Capital');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_publication`
--

DROP TABLE IF EXISTS `information_publication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_publication` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombreEdificio` varchar(45) NOT NULL,
  `piso` varchar(45) NOT NULL,
  `banos` varchar(45) NOT NULL,
  `cuartos` varchar(45) NOT NULL,
  `metros` varchar(45) NOT NULL,
  `antiguedad` varchar(45) DEFAULT NULL,
  `estacionamiento` varchar(45) DEFAULT '0',
  `costo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_publication`
--

LOCK TABLES `information_publication` WRITE;
/*!40000 ALTER TABLE `information_publication` DISABLE KEYS */;
INSERT INTO `information_publication` VALUES (6,'Manantial','2','4','4','175','10','2','17000000'),(7,'Manantial','2','11','2','11','10','1','17000000'),(8,'1','1','1','1','1','1','1','1'),(12,'34134','67','67','67','67','76','6','67');
/*!40000 ALTER TABLE `information_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1446998229);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owner_information`
--

DROP TABLE IF EXISTS `owner_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owner_information` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cedula` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `telefonoLocal` varchar(45) NOT NULL,
  `telefonoCelular` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owner_information`
--

LOCK TABLES `owner_information` WRITE;
/*!40000 ALTER TABLE `owner_information` DISABLE KEYS */;
INSERT INTO `owner_information` VALUES (4,'6161616','CONSUELO.SALAS40@GMAIL.COM','Consuelo','02127304629','02127813270'),(5,'2','1111','2','1','02127813270'),(6,'1','1','1','1','1'),(10,'564','654','516565465','65','654');
/*!40000 ALTER TABLE `owner_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacion`
--

DROP TABLE IF EXISTS `publicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_information` int(10) NOT NULL,
  `id_tipo_anuncio` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `information` int(10) NOT NULL,
  `create_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `id_status_publicacion` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_tipo_anuncio` (`id_tipo_anuncio`),
  KEY `id_status_publicacion` (`id_status_publicacion`),
  KEY `id_ciudad` (`id_ciudad`),
  KEY `information` (`information`),
  KEY `owner_information` (`owner_information`),
  CONSTRAINT `id_ciudad` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id`),
  CONSTRAINT `id_status_publicacion` FOREIGN KEY (`id_status_publicacion`) REFERENCES `status_publicacion` (`id`),
  CONSTRAINT `id_tipo_anuncio` FOREIGN KEY (`id_tipo_anuncio`) REFERENCES `tipo_anuncio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacion`
--

LOCK TABLES `publicacion` WRITE;
/*!40000 ALTER TABLE `publicacion` DISABLE KEYS */;
INSERT INTO `publicacion` VALUES (32,4,2,2,6,'2015-11-23',NULL,1),(33,5,2,2,7,'2015-11-28',NULL,1),(34,6,2,2,8,'2015-12-09',NULL,1),(38,10,2,2,12,'2015-12-12',NULL,1);
/*!40000 ALTER TABLE `publicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacion_fotos`
--

DROP TABLE IF EXISTS `publicacion_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacion_fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacion` int(11) DEFAULT NULL,
  `fotos` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='fotos de las publicaciones	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacion_fotos`
--

LOCK TABLES `publicacion_fotos` WRITE;
/*!40000 ALTER TABLE `publicacion_fotos` DISABLE KEYS */;
INSERT INTO `publicacion_fotos` VALUES (1,38,'{\"1\":\"uploads\\/2_38_1.jpg\",\"2\":\"uploads\\/2_38_2.jpg\"}');
/*!40000 ALTER TABLE `publicacion_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_publicacion`
--

DROP TABLE IF EXISTS `status_publicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_publicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_publicacion`
--

LOCK TABLES `status_publicacion` WRITE;
/*!40000 ALTER TABLE `status_publicacion` DISABLE KEYS */;
INSERT INTO `status_publicacion` VALUES (1,'Activa'),(2,'Finalizada');
/*!40000 ALTER TABLE `status_publicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_anuncio`
--

DROP TABLE IF EXISTS `tipo_anuncio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_anuncio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_anuncio` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_anuncio`
--

LOCK TABLES `tipo_anuncio` WRITE;
/*!40000 ALTER TABLE `tipo_anuncio` DISABLE KEYS */;
INSERT INTO `tipo_anuncio` VALUES (1,'Alquiler'),(2,'Venta');
/*!40000 ALTER TABLE `tipo_anuncio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `kind_user` int(11) DEFAULT NULL,
  `CI` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`CI`),
  UNIQUE KEY `nickname_UNIQUE` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'condominio'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-12 17:39:27
