<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\UploadImages;
use app\models\Ciudades;
use app\models\Estados;
use app\models\Publicacion;
use app\models\OwnerInformation;
use app\models\InformationPublication;
use app\models\User;
use yii\web\UploadedFile;
use app\models\TipoAnuncio;
use kartik\growl\Growl;
use app\components\AccessRule;

class PublicarController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    
    public function actionUpload()
    {
        $rol = Yii::$app->user->identity->type_user_id;
        if($rol == 1)
        {
            $model = new UploadImages();
            $modelCiudades = new Ciudades();
            $modelEstado = Estados::find()->orderBy('id')->all();
            $modelPublcacion = new Publicacion;
            $estado = new Estados;
            $ciudades = new Ciudades;
            $tipo = new TipoAnuncio();
            $modelTipoPublicacion = TipoAnuncio::find()->orderBy('id')->all();
            if (Yii::$app->request->isPost) 
            {
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try
                {

                    $post = Yii::$app->request->post();
                    $modelInformationOwner = new OwnerInformation();
                    $modelInformation = new InformationPublication();
                    $modelInformation->nombreEdificio = $post["Publicacion"]["nombreEdificio"];
                    $modelInformation->piso = $post["Publicacion"]["piso"];
                    $modelInformation->costo = $post["Publicacion"]["costo"];
                    $modelInformation->metros = $post["Publicacion"]["metros"];
                    $modelInformation->cuartos = $post["Publicacion"]["cuartos"];
                    $modelInformation->banos = $post["Publicacion"]["banos"];
                    $modelInformation->estacionamiento = $post["Publicacion"]["estacionamiento"];
                    $modelInformation->antiguedad = $post["Publicacion"]["antiguedad"];
                    if ($modelInformation->save())
                    {

                        $modelPublcacion->information = $modelInformation->id;
                        $modelInformationOwner->nombre=  $post["Publicacion"]["nombre"];
                        $modelInformationOwner->cedula=  $post["Publicacion"]["cedula"];
                        $modelInformationOwner->telefonoLocal=  $post["Publicacion"]["telefonoLocal"];
                        $modelInformationOwner->telefonoCelular=  $post["Publicacion"]["telefonoCelular"];
                        $modelInformationOwner->correo=  $post["Publicacion"]["correo"];
                        if($modelInformationOwner->save())
                        {   
                            $modelPublcacion->owner_information = $modelInformationOwner->id;
                            $modelPublcacion->id_ciudad=$post["Ciudades"]["Ciudades"];
                            $modelPublcacion->id_tipo_anuncio = $post["TipoAnuncio"]["tipo_anuncio"];
                            $modelPublcacion->create_date = date("Y-m-d");
                            if($modelPublcacion->save())
                            {
                                $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                                if ($model->upload($modelPublcacion->id,$post["Ciudades"]["Ciudades"])) 
                                {

                                    $transaction->commit();
                                    $model = new UploadImages();
                                    $modelCiudades = new Ciudades();
                                    $modelEstado = Estados::find()->orderBy('id')->all();
                                    $modelPublcacion = new Publicacion;
                                    $estado = new Estados;
                                    $ciudades = new Ciudades;
                                    $tipo = new TipoAnuncio();
                                    $modelTipoPublicacion = TipoAnuncio::find()->orderBy('id')->all();
                                    \Yii::$app->getSession()->setFlash('success',[
                                                                        'type' => Growl::TYPE_SUCCESS,
                                                                        'title' => 'Guardado',
                                                                        'icon' => 'glyphicon glyphicon-info-sign',
                                                                        'body' => 'Se ha guardado todo con exito',
                                                                        'showSeparator' => true,
                                                                        'delay' => 1500,
                                                                        'pluginOptions' => [
                                                                            'showProgressbar' => true,
                                                                        'placement' => [
                                                                            'from' => 'top',
                                                                            'align' => 'right',
                                                                            ]
                                                                        ]
                                                                ]);
                                }
                                else
                                {
                                    $transaction->rollBack();
                                    \Yii::$app->getSession()->setFlash('error', [
                                                                        'type' => Growl::TYPE_WARNING,
                                                                        'title' => 'Error',
                                                                        'icon' => 'glyphicon glyphicon-info-sign',
                                                                        'body' => 'Falla al subir las imagenes',
                                                                        'showSeparator' => true,
                                                                        'delay' => 1500,
                                                                        'pluginOptions' => [
                                                                            'showProgressbar' => true,
                                                                        'placement' => [
                                                                            'from' => 'top',
                                                                            'align' => 'right',
                                                                            ]
                                                                        ]
                                                                ]);
                                }

                            }
                            else
                            {
                                $transaction->rollBack();
                                \Yii::$app->getSession()->setFlash('error',[
                                                                    'type' => Growl::TYPE_WARNING,
                                                                    'title' => 'Error!',
                                                                    'icon' => 'glyphicon glyphicon-info-sign',
                                                                    'body' => 'Falla General al cargar la informacion',
                                                                    'showSeparator' => true,
                                                                    'delay' => 1500,
                                                                    'pluginOptions' => [
                                                                        'showProgressbar' => true,
                                                                    'placement' => [
                                                                        'from' => 'top',
                                                                        'align' => 'right',
                                                                        ]
                                                                    ]
                                                                ]);
                            }
                        }
                        else
                        {
                            $transaction->rollBack();
                        }
                    }
                    else
                    {
                        $transaction->rollBack();
                    }

                } catch (Exception $ex) {
                    $transaction->rollBack();
                    var_dump($ex);
                }
            }

            return $this->render('upload', [
                'model' => $model,
                'modelCiudades' => $modelCiudades,
                'modelEstado' => $modelEstado,
                'estado'=>$estado,
                'ciudades'=>$ciudades,
                'tipo' => $tipo,
                'publicacion'=>$modelPublcacion,
                'tipoPublicacion'=>$modelTipoPublicacion,
                    ]);   
        }
        else
        {
            return $this->render('site/permission');
        }

    }
    
    public function actionVentas()
    {
        $ventas = Publicacion::getAllSales();
        
        if (empty($ventas))
        {
            $ventas = NULL;
        }

        
        return $this->render('ventas',[
            'ventas' => $ventas
            ]);
    }
    
    public function actionAlquiler()
    {
        $alquiler = Publicacion::getAllRents();
        
        if(empty($alquiler))
        {
            $alquiler = NULL;
        }
        
        return $this->render('alquiler',[
            'alquiler' => $alquiler
            ]);
        
    }
    
    public function actionGetinfo()
    {
        $id = $_GET["id"];
        $array = [];
        $information = Publicacion::getInformationForModal($id);
        $array["information"]["nombreEdificio"] = $information->nombreEdificio;
        $array["information"]["piso"] = $information->piso;
        $array["information"]["metros"] = $information->metros;
        $array["information"]["banos"] = $information->banos;
        $array["information"]["cuartos"] = $information->cuartos;
        $array["information"]["estacionamiento"] = $information->estacionamiento;
        $array["information"]["antiguedad"] = $information->antiguedad;
        $array["information"]["costo"] = $information->costo;
        $array["information"]["fotos"] = json_decode($information->fotos,true);
        $array["information"]["telefonoLocal"] = $information->telefonoLocal;
        $array["information"]["telefonoCelular"] = $information->telefonoCelular;
                
        return json_encode($array);
    }
}