<?php

namespace app\controllers;

use Yii;
use app\models\Estados;

class EstadosController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionCreate()
    {
        $model = new Estados();

        $post = Yii::$app->request->post('Estados');
        if ($post['estados'])
        {
           
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try     
            {
                $model = Estados::findOne(['estados'=>$post['estados']]);
                if (isset($model))
                {
                    $transaction->rollBack();
                    
                }
                else
                {
                    $model = new Estados;
                    $model->estados = $post['estados'];
                    if($model->save())
                    {
                        $transaction->commit();    
                        $model = new Estados();
                    }
                    else
                    {
                        $transaction->rollBack();
                    }
                }
                
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return $this->render('create',
                [
                 'model'   => $model
                ]);
    }
    
   
}
