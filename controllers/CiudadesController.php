<?php


namespace app\controllers;

use Yii;
use app\models\Ciudades;
use app\models\Estados;

class CiudadesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCreate()
    {
        $model= new Ciudades();
        $modelEstado = Estados::find()->orderBy('id')->all();
        
        $post = Yii::$app->request->post();
        if($post)
        {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try 
            {
                $model = Ciudades::findOne(['ciudades'=>$post['Ciudades']['ciudades'],
                                        'id_estados'=>$post['Ciudades']['Estados']]);
                if(!isset($model))
                {
                    $model = new Ciudades;
                    $model->id_estados = $post['Ciudades']['Estados'];
                    $model->ciudades = $post['Ciudades']['ciudades'];
                    if($model->save())
                    {
                        $transaction->commit();    
                       $model= new Ciudades();
                    }
                    else
                    {
                        $transaction->rollBack();
                    }
                }
                else
                {
                    $transaction->rollBack();
                }
            }
            catch (Exception $ex) 
            {
                $transaction->rollBack();
                return $ex;
            }
            
        }
        return $this->render('create',
                [
                    'model'   => $model,
                    'estados' => $modelEstado,
                ]);
    }
    
    public function actionBringstates($id)
    {
        $id_Estado = $_GET["id"];
        $model = Ciudades::getCiudadesByID($id_Estado);
        
        return (json_encode($model));
    }
            
}
